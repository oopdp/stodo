<@content for="title">Simple Web App</@content>
<ul>
    <li><@link_to controller="books">Books CRUD example</@link_to></li>
    <li><@link_to controller="greeting">Dependency injection example</@link_to></li>
	<li><@link_to controller="/sbadmin/sample">SB Admin 2.0 Sample</@link_to></li>
	<li><@link_to controller="home" action="demo">Sample Bootstrap</@link_to></li>
	<li><@link_to controller="/stodo/home">Todo</@link_to></li>
</ul>