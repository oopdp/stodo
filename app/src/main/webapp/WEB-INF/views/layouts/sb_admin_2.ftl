<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<title>ActiveWeb - <@yield to="title"/></title>
		<#include "sb_admin_2/default_css.ftl" >
	</head>
	<body>
		<div id="wrapper">
			<#include "sb_admin_2/navbar.ftl" >
			<!-- Page Content -->
			<div id="page-wrapper">
				${page_content}
			</div>
			<!-- /#page-wrapper -->
		</div>
		<!-- /#wrapper -->
		<#include "sb_admin_2/default_js.ftl" >
		<@yield to="js"/>
	</body>
</html>
