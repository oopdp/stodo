<#assign base_css="${context_path}/components/sb-admin-2-1.0.8">
<!-- Bootstrap Core CSS -->
<link href="${context_path}/components/sb-admin-2-1.0.8/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>

<!-- MetisMenu CSS -->
<link href="${context_path}/components/sb-admin-2-1.0.8/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

<!-- Timeline CSS -->
<link href="${context_path}/components/sb-admin-2-1.0.8/dist/css/timeline.css" rel="stylesheet">

<!-- DataTables CSS -->
<link href="${base_css}/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<!--<link href="${base_css}/bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">-->

<!-- Social Buttons CSS -->
<link href="${base_css}/bower_components/bootstrap-social/bootstrap-social.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="${context_path}/components/sb-admin-2-1.0.8/dist/css/sb-admin-2.css" rel="stylesheet" type="text/css"/>

<!-- Morris Charts CSS -->
<link href="${context_path}/components/sb-admin-2-1.0.8/bower_components/morrisjs/morris.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="${context_path}/components/sb-admin-2-1.0.8/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
