<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
	<#include "branding.ftl" >
	<#include "topbar.ftl" >
	<#include "sidebar.ftl" >
</nav>