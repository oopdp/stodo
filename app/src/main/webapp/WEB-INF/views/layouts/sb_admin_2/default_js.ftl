<@yield to="use_morris_js"/>
<@yield to="use_flot_js"/>
<#assign base_js="${context_path}/components/sb-admin-2-1.0.8">
<!-- jQuery -->
<script src="${context_path}/components/sb-admin-2-1.0.8/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="${context_path}/components/sb-admin-2-1.0.8/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="${context_path}/components/sb-admin-2-1.0.8/bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- DataTables JavaScript -->
<script src="${base_js}/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="${base_js}/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
	
<#if (use_morris_js == "1")??>
	<!-- Morris Charts JavaScript -->
	<script src="${context_path}/components/sb-admin-2-1.0.8/bower_components/raphael/raphael-min.js"></script>
	<script src="${context_path}/components/sb-admin-2-1.0.8/bower_components/morrisjs/morris.min.js"></script>
	<script src="${context_path}/components/sb-admin-2-1.0.8/js/morris-data.js"></script>
</#if>

<#if (use_flot_js == "1")??>
	<!-- Flot Charts JavaScript -->
	<script src="${base_js}/bower_components/flot/excanvas.min.js"></script>
	<script src="${base_js}/bower_components/flot/jquery.flot.js"></script>
	<script src="${base_js}/bower_components/flot/jquery.flot.pie.js"></script>
	<script src="${base_js}/bower_components/flot/jquery.flot.resize.js"></script>
	<script src="${base_js}/bower_components/flot/jquery.flot.time.js"></script>
	<script src="${base_js}/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
	<script src="${base_js}/js/flot-data.js"></script>
</#if>

<!-- Custom Theme JavaScript -->
<script src="${context_path}/components/sb-admin-2-1.0.8/dist/js/sb-admin-2.js"></script>
