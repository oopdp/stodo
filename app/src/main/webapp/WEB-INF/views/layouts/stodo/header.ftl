<div class="header">
    <h1><a href="${context_path}">STodo - Silly ToDo List</a> </h1>
	<div class="pull-right">
		<#if (session.user) ??>
			<a href="javascript:void(0);" ">Logged in as: ${session.user.username}</a> |
			<@link_to controller="/stodo/login" action="logout">Logout</@>
		<#else>
			<@link_to controller="/stodo/home" >Login</@>
		</#if>
	</div>
</div>

