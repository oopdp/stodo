<div class="row">
	<div class="col-md-12">
		<#if (flasher.message) ??>
			<div class="alert alert-warning alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<strong>Warning!</strong> <@flash name="message"/>
			</div>
		</#if>
	</div>	
</div>