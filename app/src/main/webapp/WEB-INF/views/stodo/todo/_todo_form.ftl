<@form controller="/stodo/todo" action="add" method="post">
	<form role="form">
	<div class="form-group">
		<input type="text" class="form-control" placeholder="Name" name="name">
	</div>
	<div class="form-group">
		<textarea type="text" name="content" class="form-control" rows="3" placeholder="Content"></textarea>
	</div>
	<input type="hidden" name="scope_id" value="${scope.id}">	
	<button type="submit" class="btn btn-default">Add</button>
</@form>