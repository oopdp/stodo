<@content for="title">Todo</@>
<div class="row">
	<div class="col-md-12">
		<div class="page-header">
			<h1>${scope.name}</h1>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-2">
		<@render partial="todo_form" scope=scope/>
		<hr>
	</div>
	<div class="col-md-10">
		<div class="row">
			<div class="col-md-2">
				<@render partial="todo_panel" collection=todos spacer="todo_panel_spacer"/>
			</div>
		</div>	
	</div>
</div>