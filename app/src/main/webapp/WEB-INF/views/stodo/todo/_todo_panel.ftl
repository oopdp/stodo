<#assign ver = todo_panel.todo.getOptionValue("ver") >
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">${todo_panel.todo.name?web_safe}</h3>
	</div>
	<div class="panel-body">
		${todo_panel.todo.renderContent()?web_safe}
	</div>
</div>
