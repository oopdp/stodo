<@content for="title">Welcome</@>
<div class="jumbotron">
	<h1>Do you need a Todo List?</h1>
	<p>I doubt it, but here it is and it's free.</p>
</div>
<div class="row">
	<div class="col-md-1"></div>
	<div class="col-md-4">
		<div class="page-header">
			<h1>Sign In</h1>
		</div>
		<@render partial="signin_form"/>
	</div>
	<div class="col-md-2"></div>
	<div class="col-md-4">
		<div class="page-header">
			<h1>Sign Up</h1>
		</div>
		<@render partial="signup_form"/>
	</div>
	<div class="col-md-1"></div>
</div>
