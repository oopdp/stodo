/*
 * Here comes the text of your license (this is my license)
 * Each line should be prefixed with  * 
 */
package biz.igg.oopdp.exp.auth;

/**
 *
 * @author Random Email
 */
import app.controllers.stodo.LoginController;
import org.javalite.activeweb.controller_filters.HttpSupportFilter;

/**
 * @author Igor Polevoy on 9/29/14.
 */
public class AuthorizationFilter extends HttpSupportFilter {
    @Override
    public void before() {

        if(!controllerProtected()){
            return;// allow to fall to controller
        }

        if(!sessionHas("user") && controllerProtected()){
            redirect(LoginController.class);
        }
    }

    private boolean controllerProtected() {
        return getRoute().getController().getClass().getAnnotation(Protected.class) != null;
    }
}