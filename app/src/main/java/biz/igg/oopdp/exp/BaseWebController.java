/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.exp;

import java.util.Map;
import org.javalite.activeweb.AppController;
import java.io.Serializable;
import app.models.stodo.User;

/**
 *
 * @author Random Email
 */
public class BaseWebController extends AppController {
	
	public static String userAuth = "user";
	
	protected void setMessage(String type, Object value) {
        super.flash("message", value);
    }
	
	protected void authUser(Serializable resource)
	{
		super.session(userAuth, resource);
	}
	
	
	protected User getCurrentUser()
	{
		return (User) super.session(userAuth);
	}
	
	protected void deAuthUser()
	{
		super.session(userAuth, null);
	}
	
//	protected 
	
	protected void setRedirect(String redirect) {
        super.redirect(redirect);
    }
	
}
