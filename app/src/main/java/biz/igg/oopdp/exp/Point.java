/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.exp;

/**
 *
 * @author Random Email
 */
public class Point {
	
	private double x;
	private double y;
	
	/**
	 * 
	 * @param x
	 * @param y 
	 */
	public Point(double x, double y)
	{
		this.x = x;
		this.y = y;
	}
	
	/**
	 * 
	 * @param p 
	 */
	public Point(Point p)
	{
		this.x = p.getX();
		this.y = p.getY();
	}
	
	
	/**
	 * 
	 * @return 
	 */
	public String doMe()
	{
		return "("+x+","+y+")";
	}
	
	/**
	 * 
	 * @return 
	 */
	public String doMeAgain()
	{
		return "("+x+","+y+")";
	}
	
	/**
	 * 
	 * @return 
	 */
	public String doMeAgainAgain()
	{
		return "("+x+","+y+")";
	}
	
	public String doMeAgainAgain2()
	{
		return "("+x+","+y+")";
	}
	
	
	public String doMeAgainAgain3()
	{
		return "("+x+","+y+")";
	}
	
	public void swap(Point p1, Point p2)
	{
		Point tmp = p1;
		p1 = p2;
		p2 = tmp;
	}
	
	public void alter(Point p1, Point p2)
	{
		Point tmp = new Point(p2);
		p2.setX(p1.getX());
		p2.setY(p1.getY());
		p1.setX(tmp.getX());
		p1.setY(tmp.getY());
	}
	
	public void swapCoordinates(Point p)
	{
		double tmp = p.getX();
		p.setX(p.getY());
		p.setY(tmp);
	}
	
	
	@Override
	public String toString()
	{
		return doMe();
	}
	
	public void setX(double x)
	{
		this.x = x;
	}
	
	public void setY(double y)
	{
		this.y = y;
	}
	
	
	
	public double getX()
	{
		return x;
	}
	
	public double getY()
	{
		return y;
	}
}
