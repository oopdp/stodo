/*
 * Here comes the text of your license (this is my license)
 * Each line should be prefixed with  * 
 */
package biz.igg.oopdp.exp.auth;


import java.lang.annotation.*;

/**
 * 
 * @author Random Email
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Inherited
public @interface Protected {}