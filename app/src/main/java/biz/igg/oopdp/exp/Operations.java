/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.exp;

import java.util.Map;
import app.models.stodo.User;
import app.models.stodo.Scope;
import app.models.stodo.ScopesTodos;
import app.models.stodo.ScopesUsers;
import app.models.stodo.Todo;
import biz.igg.oopdp.exp.auth.PasswordEncryptionServiceWrapper;
import java.util.List;

/**
 * Some God-like class..
 * 
 * @author salvix
 */
public class Operations {
	
	/**
	 * 
	 * @param map
	 * @return 
	 */
	public static OpOutput authUserFromParams(Map<String, String> map)
	{
		OpOutput o = new OpOutput();
		o.setRedirect("/stodo/home");
		o.setMessage("Sorry, credentials are not valid");
		if(map.get("password").isEmpty() || map.get("email").isEmpty()){
			o.setMessage("Please type your credentials");
			return o;
		}
		User u = User.first("email = ? order by id DESC", map.get("email"));
		if(u == null){
			
		}
		else {
			if(PasswordEncryptionServiceWrapper.match(
				(String) map.get("password"), 
				(String) u.get("password"), 
				(String) u.get("password_salt")
			)){
				o.setError(false);
				o.setObject(u);
				o.setMessage("Welcome back");
				o.setRedirect("/stodo/todo");
			}
		}
		return o;
	}
	
	
	/**
	 * 
	 * @return 
	 */
	public static OpOutput deAuthUser()
	{

		OpOutput o = new OpOutput();
		o.setRedirect("/stodo/home");
		o.setMessage("Thanks and see you soon!");
		return o;
	}
	
	public static OpOutput getUserDefaultScope(User u)
	{
		OpOutput o = new OpOutput();
		Scope s = Scope.first("created_by = ? order by id DESC", u.getId());
		if(s == null){
			s = (Scope) buildUserDefaultScope(u).getObject();
		}
		o.setObject(s);
		return o;
	}
	
	public static OpOutput buildUserDefaultScope(User u)
	{
		OpOutput o = new OpOutput();
		Scope s = new Scope();
		s.set("name", "Todo");
		s.set("created_by", u.getId());
		s.set("content", "Default Todo List");
		s.set("options", s.defaultOptions());
		if(s.save()){
			ScopesUsers su = new ScopesUsers();
			su.set("scope_id", s.getId());
			su.set("user_id", u.getId());
			su.set("created_by", u.getId());
			su.set("options", s.defaultOptions());
			su.save();
		}
		o.setObject(s);
		return o;
	}
	
	public static void log(Object s)
	{
		Util.log(s);
	}
	
	
	/**
	 * 
	 * @return 
	 */
	public static OpOutput getUserDashboard(User u)
	{
		OpOutput o = new OpOutput();
		OpOutput so = getUserDefaultScope(u);
		Scope s = (Scope) so.getObject();
		o.appendItemToChain("scope", so);
		List<ScopesTodos> sts = ScopesTodos.
			where("scope_id = ? ", s.getId()).
			orderBy("position ASC").
			include(Todo.class);
		o.setObject(sts);
		return o;
	}
	
	/**
	 * 
	 * @param u
	 * @param s
	 * @param map
	 * @return 
	 */
	public static OpOutput createTodoFromParams(User u, Scope s, Map<String, String> map)
	{
		OpOutput o = new OpOutput();
		if(!map.get("name").isEmpty() && !map.get("content").isEmpty()){
			Todo t = new Todo();
			t.set("name", map.get("name"));
			t.set("created_by", u.getId());
			t.set("content", map.get("content"));
			t.set("options", t.defaultOptions());
			t.set("content_type", "");
			if(t.save()){
				ScopesTodos st = new ScopesTodos();
				st.set("scope_id", s.getId());
				st.set("todo_id", t.getId());
				st.set("created_by", u.getId());
				st.set("options", t.defaultOptions());
				o.setError(!st.save());
				o.setMessage("Todo created successfully!");
				o.setObject(t);
			}
		}
		o.setRedirect("/stodo/todo");
		return o;
	}
	
	
	/**
	 * 
	 * @param u
	 * @param s
	 * @param map
	 * @return 
	 */
	public static OpOutput createTodoFromParams(User u, Map<String, String> map)
	{
		if(!map.get("scope_id").isEmpty()){
			Scope s = Scope.first("id = ?", map.get("scope_id"));
			if(s.exists()){
				return createTodoFromParams(u, s, map);
			}
		}
		OpOutput o = new OpOutput();
		o.setRedirect("/stodo/todo");
		return o;
	}
	
	
	public static void alterUserParamsForEncryption(Map<String, String> map)
	{
		// ENC password here!
		String salt = PasswordEncryptionServiceWrapper.generateSalt();
		String encPwd = PasswordEncryptionServiceWrapper.encrypt(map.get("password"), salt);
		map.put("password_salt", salt);
		map.put("password", encPwd);
		// ENC COMPLETED
	}
	
	/**
	 * 
	 * @param map
	 * @return 
	 */
	public static OpOutput createUserFromParams(Map<String, String> map)
	{
		OpOutput o = new OpOutput();
		o.setRedirect("/stodo/home");
		if(map.get("password").equals(map.get("password_rep"))){
			User u = new User();
			alterUserParamsForEncryption(map);
			map.remove("password_rep");
			map.put("confirmation_code", User.generateConfirmationCode());
			map.put("confirmed", "0");
			map.put("timezone", "UTC");
			map.put("language", "en");
			u.fromMap(map);
			o.setMessage("Please log in after activating your account.");
			o.setError(!u.save());
			o.setObject(u);
			return o;
		}
		o.setError(true);
		o.setMessage("Please make sure you retyped pwd correctly");
		return o;
	}
}
