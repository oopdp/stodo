/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.exp;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author salvix
 */
public class Util {
	
	/**
	 * 
	 * @return String
	 */
	public static String uuid()
	{
		return java.util.UUID.randomUUID().toString();
	}
	
	/**
	 * 
	 * @param s 
	 */
	public static void log(Object s)
	{
		System.out.println(s);
	}
	
	public static String[] getTimezones()
	{
		return java.util.TimeZone.getAvailableIDs();
	}
	
	public static JSONObject toJson(String s)
	{
		JSONParser parser = new JSONParser();
		JSONObject json;
		try {
			json = (JSONObject) parser.parse(s);
		}
		catch(ParseException e){
			json = new JSONObject();
		}
		return json;
	}
	
	public static String fromJson(JSONObject json)
	{
		return json.toString();
	}
	
}
