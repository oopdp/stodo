/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.exp.auth;

import biz.igg.oopdp.exp.auth.PasswordEncryptionService;

/**
 *
 * @author salvix
 */
public class PasswordEncryptionServiceWrapper {
	
	/**
	 * 
	 * @return 
	 */
	public static String generateSalt()
	{
		PasswordEncryptionService p = new PasswordEncryptionService();
		try {
			return encode(p.generateSalt());
		}
		catch(Exception e){
			
		}
		return null;
	}
	
	/**
	 * 
	 * @param password
	 * @param salt
	 * @return 
	 */
	public static String encrypt(String password, String salt)
	{
		PasswordEncryptionService p = new PasswordEncryptionService();
		try {
			byte[] saltD = decode(salt);
			byte[] enc = p.getEncryptedPassword(password, saltD);
			return encode(enc);
		}
		catch(Exception e){
			
		}
		return null;
	}
	
	/**
	 * 
	 * @param attemptedPassword
	 * @param encPassword
	 * @param salt
	 * @return 
	 */
	public static boolean match(String attemptedPassword, String encPassword, String salt)
	{
		PasswordEncryptionService p = new PasswordEncryptionService();
		byte[] encPasswordD = decode(encPassword);
		byte[] saltD = decode(salt);
		try {
			return p.authenticate(attemptedPassword, encPasswordD, saltD);
		}
		catch(Exception e){
			
		}
		return false;
	}
	
	private static String encode(byte[] bytes)
	{
		return java.util.Base64.getEncoder().encodeToString(bytes);
	}
	
	private static byte[] decode(String bytes)
	{
		return java.util.Base64.getDecoder().decode(bytes);
	}
	
}
