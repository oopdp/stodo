/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.exp;

import java.util.HashMap;

/**
 *
 * @author salvix
 */
public class OpOutput {

	private	Object o = null;
	private String message = "Sorry, an error has occured";
	private String messageType = "warning";
	private String redirect = null;
	private boolean error = true;
	private HashMap<String, OpOutput> chain;
	
	/**
	 * 
	 * @param o
	 * @param message
	 * @param messageType
	 * @param redirect 
	 */
	public OpOutput(Object o, String message, String messageType, String redirect){
		this.o = o;
		this.message = message;
		this.messageType = messageType;
		this.redirect = redirect;
		initChain();
	}
	
	private void initChain()
	{
		chain = new HashMap<String, OpOutput>();
	}
	
	
	public OpOutput(Object o, String message)
	{
		this.o = o;
		this.message = message;
		initChain();
	}
	
	public OpOutput(Object o)
	{
		this.o = o;
		initChain();
	}
	
	public OpOutput()
	{
		initChain();
	}
	
	
	
	public Object getObject()
	{
		return o;
	}
	
	public String getMessage()
	{
		return message;
	}
	
	public String getMessageType()
	{
		return messageType;
	}
	
	
	public String getRedirect()
	{
		return redirect;
	}
	
	
	/**
	 * 
	 * @return 
	 */
	public HashMap<String, OpOutput> getChain()
	{
		return chain;
	}
	
	public void appendItemToChain(String key, OpOutput o)
	{
		chain.put(key, o);
	}
	
	public OpOutput getChainItem(String key)
	{
		return chain.get(key);
	}
	
	public boolean getError()
	{
		return error;
	}
	
	public void setObject(Object o)
	{
		this.o = o;
	}
	
	public void setMessage(String m)
	{
		message = m;
	}
	
	public void setMessageType(String mt)
	{
		messageType = mt;
	}
	
	public void setError(boolean err)
	{
		error = err;
	}
	
	public void setRedirect(String r)
	{
		redirect = r;
	}
	
	
	public HashMap<String, String> toMap()
	{	
		HashMap<String, String> m = new HashMap<String, String>();
		m.put("object", getObject().toString());
		m.put("message", getMessage());
		m.put("message_type", getMessageType());
		m.put("redirect", getRedirect());
		m.put("error", String.valueOf(getError()));
		return m;
	}
	
	@Override
	public String toString()
	{
		HashMap<String, String> m = toMap();
		return m.toString();
	}
	
}
