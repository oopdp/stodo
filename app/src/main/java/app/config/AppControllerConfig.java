/*
Copyright 2009-2010 Igor Polevoy 

Licensed under the Apache License, Version 2.0 (the "License"); 
you may not use this file except in compliance with the License. 
You may obtain a copy of the License at 

http://www.apache.org/licenses/LICENSE-2.0 

Unless required by applicable law or agreed to in writing, software 
distributed under the License is distributed on an "AS IS" BASIS, 
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
See the License for the specific language governing permissions and 
limitations under the License. 
*/
package app.config;

import org.javalite.activeweb.AbstractControllerConfig;
import org.javalite.activeweb.AppContext;
import org.javalite.activeweb.controller_filters.DBConnectionFilter;
import org.javalite.activeweb.controller_filters.TimingFilter;
import app.controllers.BooksController;

import biz.igg.oopdp.exp.auth.AuthorizationFilter;
import app.controllers.stodo.LoginController;
import app.controllers.stodo.TodoController;

/**
 * @author Igor Polevoy
 */
public class AppControllerConfig extends AbstractControllerConfig {

    public void init(AppContext context) {
        addGlobalFilters(new TimingFilter());
		addGlobalFilters(new AuthorizationFilter());
        add(new DBConnectionFilter()).to(BooksController.class);
		// this is a bit sloppy...
//		add(new DBConnectionFilter()).to(BooksController.class);
		add(new DBConnectionFilter()).to(LoginController.class);
		add(new DBConnectionFilter()).to(TodoController.class);
    }
}
