/*
 * Here comes the text of your license (this is my license)
 * Each line should be prefixed with  * 
 */
package app.controllers.stodo;
import biz.igg.oopdp.exp.BaseWebController;
/**
 *
 * @author Random Email
 */
abstract public class BaseController extends BaseWebController {
	
	@Override
	protected String getLayout(){
		return "/layouts/stodo/stodo";
	}
	
}
