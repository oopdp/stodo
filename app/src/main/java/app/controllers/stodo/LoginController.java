/*
 * Here comes the text of your license (this is my license)
 * Each line should be prefixed with  * 
 */
package app.controllers.stodo;
import org.javalite.activeweb.annotations.POST;
import org.javalite.common.Collections;
import app.models.stodo.User;
import biz.igg.oopdp.exp.OpOutput;
import biz.igg.oopdp.exp.Operations;

/**
 *
 * @author Random Email
 */
public class LoginController extends BaseController {
	
	public void index(){
		setRedirect("/stodo/home");
	}
	
    @POST
    public void login(){
		OpOutput op = Operations.authUserFromParams(params1st());
		if(!op.getError()){
			User u = (User) op.getObject();
			authUser(u);
		}
		setMessage(op.getMessageType(), op.getMessage());
		setRedirect(op.getRedirect());
    }
	
	@POST
	public void signup(){
		OpOutput op = Operations.createUserFromParams(params1st());
		setMessage(op.getMessageType(), op.getMessage());
		redirect(op.getRedirect());
	}

    public void logout(){
		OpOutput op = Operations.deAuthUser();
		deAuthUser();
		setMessage(op.getMessageType(), op.getMessage());
		setRedirect(op.getRedirect());
    }
}
