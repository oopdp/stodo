/*
 * Here comes the text of your license (this is my license)
 * Each line should be prefixed with  * 
 */
package app.controllers.stodo;
import biz.igg.oopdp.exp.auth.Protected;
import org.javalite.activeweb.annotations.POST;
import app.models.stodo.User;
import app.models.stodo.Scope;
import app.models.stodo.ScopesTodos;
import biz.igg.oopdp.exp.Util;
import biz.igg.oopdp.exp.OpOutput;
import biz.igg.oopdp.exp.Operations;
import java.util.List;

/**
 *
 * @author Random Email
 */

@Protected
public class TodoController extends BaseController {
	
	public void index(){
		User u = (User) this.getCurrentUser();
		OpOutput op = Operations.getUserDashboard(u);
		assign("todos", (List<ScopesTodos>)op.getObject());
		Scope s = (Scope) op.getChainItem("scope").getObject();
		assign("scope", s);
	}
	
	@POST
	public void add(){
		User u = (User) this.getCurrentUser();
		Scope s;
		s = (Scope) Operations.getUserDefaultScope(u).getObject();
		OpOutput op = Operations.createTodoFromParams(u, s, params1st());
		setMessage(op.getMessageType(), op.getMessage());
		setRedirect(op.getRedirect());
	}
}
