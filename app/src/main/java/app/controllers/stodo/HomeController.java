/*
 * Here comes the text of your license (this is my license)
 * Each line should be prefixed with  * 
 */
package app.controllers.stodo;

import app.models.stodo.User;

/**
 *
 * @author Random Email
 */
public class HomeController extends BaseController {
	
	public void index(){
		User u = getCurrentUser();
		if(u != null){
			setRedirect("/stodo/todo");
		}
	}
}
