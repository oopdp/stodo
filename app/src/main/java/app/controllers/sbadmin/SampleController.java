/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers.sbadmin;
import biz.igg.oopdp.exp.BaseWebController;
/**
 *
 * @author Random Email
 */
public class SampleController extends BaseWebController {
	
	@Override
	protected String getLayout(){
		return "/layouts/sb_admin_2";
	}
	
	public void index(){}
	
	public void flot(){}
	
	public void morris(){}
	
	public void charts(){}
	
	public void tables(){}
	
	public void forms(){}
	
	public void panels(){}
	
	public void buttons(){}
	
	public void blank(){}
	
	public void notifications(){}
	
	public void typography(){}
	
	public void icons(){}
	
	public void grid(){}
	
	public void login(){
		render().noLayout();
	}
	
	
}
