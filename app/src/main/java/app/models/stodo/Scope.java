/*
 * Here comes the text of your license (this is my license)
 * Each line should be prefixed with  * 
 */
package app.models.stodo;

import biz.igg.oopdp.exp.Util;
import org.json.simple.JSONObject;

/**
 *
 * @author Random Email
 */
public class Scope extends Base {
	
	
	public String defaultOptions()
	{
		JSONObject j = new JSONObject();
		j.put("ver", 1);
		return Util.fromJson(j);
	}
	
	
}
