/*
 * Here comes the text of your license (this is my license)
 * Each line should be prefixed with  * 
 */
package app.models.stodo;

import biz.igg.oopdp.exp.Util;
import org.json.simple.JSONObject;

/**
 *
 * @author Random Email
 */
public class Todo extends Base {
	
	
	public String defaultOptions()
	{
		JSONObject j = new JSONObject();
		j.put("ver", 1);
		return Util.fromJson(j);
	}
	
	public JSONObject getOptions()
	{
		return Util.toJson((String) get("options"));
	}
	
	public String getOptionValue(String key)
	{
		return String.valueOf(getOptions().get(key));
	}
	
	public String renderContent()
	{
		return (String) get("content");
	}
}
