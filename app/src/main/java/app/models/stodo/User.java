/*
 * Here comes the text of your license (this is my license)
 * Each line should be prefixed with  * 
 */
package app.models.stodo;

import java.util.Map;
import biz.igg.oopdp.exp.OpOutput;
import biz.igg.oopdp.exp.Util;
/**
 *
 * @author Random Email
 */
public class User extends Base {
	
	static {
		validatePresenceOf("email");
        validateEmailOf("email");
    }
	
	public static String generateConfirmationCode()
	{
		return Util.uuid();
	}

}
