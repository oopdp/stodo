/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.exp;

import biz.igg.oopdp.exp.auth.PasswordEncryptionServiceWrapper;
import biz.igg.oopdp.exp.auth.PasswordEncryptionService;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author salvix
 */
public class PasswordEncryptionServiceTest {
	
	
	
	@Test
	public void PasswordEncryptionService()
	{
		String pwd = "my_pwd";
		PasswordEncryptionService p = new PasswordEncryptionService();
		try {
			byte[] salt = p.generateSalt();
			byte[] enc = p.getEncryptedPassword(pwd, salt);
			Assert.assertTrue(p.authenticate(pwd, enc, salt));
		}
		catch(Exception e){
			Assert.assertTrue(false);
		}
	}
	
	@Test
	public void PasswordEncryptionServiceWrapper()
	{
		String salt = PasswordEncryptionServiceWrapper.generateSalt();
		String pwd = "this is my password";
		String encPwd = PasswordEncryptionServiceWrapper.encrypt(pwd, salt);
		Assert.assertTrue(PasswordEncryptionServiceWrapper.match(pwd, encPwd, salt));
		Assert.assertFalse(PasswordEncryptionServiceWrapper.match("another pwd", encPwd, salt));
	}
}
