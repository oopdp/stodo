/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.exp;

import app.models.Book;
import app.models.stodo.Scope;
import app.models.stodo.ScopesTodos;
import app.models.stodo.ScopesUsers;
import app.models.stodo.Todo;
import app.models.stodo.User;
import org.junit.Test;
import java.util.*;
import org.javalite.activejdbc.LazyList;

/**
 *
 * @author salvix
 */
public class ModelsSpec extends CustomDBSpec {
	
	
	@Override
	public void beforeTest()
	{
//		Util.log("BEFORE IN TEST");
	}
	
	
	@Override
	public void afterTest()
	{
//		Util.log("AFTER IN TEST");
//		super.afterTest();
	}
	
	@Test
    public void shouldValidateBookRequiredAttributes(){
        Book book = new Book();
        a(book).shouldNotBe("valid");
        book.set("title", "fake title", "author", "fake author", "isbn", "12345");
        a(book).shouldBe("valid");
		book.saveIt();
    }
	
	@Test
	public void createSomeBooks() {
		Book.createIt("author", "J. D. Salinger", "title", "The Catcher in the Rye", "isbn", " 9780316769174");
        Book.createIt("author", "Jared Diamond", "title", "Guns, Germs, and Steel", "isbn", "9780393061314");
		a(Book.count()).shouldBeEqual(2);
    }
	
	@Test
	public void userShouldCreate()
	{
		User u = new User();
		HashMap<String, String> m = genUserData(0);
		u.fromMap(m);
		it(u.save()).equals(true);
		User u1 = User.first("username = ?", m.get("username"));
		a(u1.get("username")).shouldBeEqual("username0");
	}
	
	
	@Test
	public void operationCreateUserFromParams()
	{
		HashMap<String, String> m = genSignUpData(5);
		OpOutput o = Operations.createUserFromParams(m);
		User u1 = User.first("username = ?", m.get("username"));
		a(u1.get("username")).shouldBeEqual("username5");
		a(User.count()).shouldBeEqual(1);
		User recd = (User)o.getObject();
		a(u1.getId()).shouldBeEqual(recd.getId());
		a(o.getError()).shouldBeFalse();
	}
	
	@Test
	public void operationAuthUserFromParams()
	{
		HashMap<String, String> m = genSignUpData(5);
		OpOutput o = Operations.createUserFromParams(m);
		User u1 = User.first("username = ?", m.get("username"));
		a(u1.get("username")).shouldBeEqual("username5");
		HashMap<String, String> mIn = genSignInData(5);
		OpOutput auth = Operations.authUserFromParams(mIn);
		User u2 = (User) auth.getObject();
		a(auth.getError()).shouldBeFalse();
		a(u2.getId()).shouldBeEqual(u1.getId());
		HashMap<String, String> mOut = genSignInData(5);
		mOut.put("password", "wrong_pwd!");
		OpOutput authFail = Operations.authUserFromParams(mOut);
		User u3 = (User) authFail.getObject();
		a(authFail.getError()).shouldBeTrue();
	}
	
	@Test
	public void operationBuildUserDefaultScope()
	{
		a(User.count()).shouldBeEqual(0);
		User u1 = genUser(0);
		a(User.count()).shouldBeEqual(1);
		OpOutput o = Operations.buildUserDefaultScope(u1);
		Scope s = (Scope) o.getObject();
		a(s.get("created_by")).shouldBeEqual(u1.getId());
	}
	
	
	@Test
	public void operationCreateTodoFromParams()
	{
		a(User.count()).shouldBeEqual(0);
		a(Scope.count()).shouldBeEqual(0);
		a(Todo.count()).shouldBeEqual(0);
		a(ScopesTodos.count()).shouldBeEqual(0);
		a(ScopesUsers.count()).shouldBeEqual(0);
		HashMap<String, String> um = genSignUpData(5);
		OpOutput o1 = Operations.createUserFromParams(um);
		User u = (User) o1.getObject();
		OpOutput o2 = Operations.buildUserDefaultScope(u);
		Scope s = (Scope) o2.getObject();
		
		HashMap<String, String> map = genTodoData(3);
		OpOutput o = Operations.createTodoFromParams(u, s, map);
		Todo t = (Todo) o.getObject();
		
		a(User.count()).shouldBeEqual(1);
		a(Scope.count()).shouldBeEqual(1);
		a(Todo.count()).shouldBeEqual(1);
		a(ScopesTodos.count()).shouldBeEqual(1);
		a(ScopesUsers.count()).shouldBeEqual(1);
		
		a(t.get("name")).shouldBeEqual("Todo 3");
		a(t.get("content")).shouldBeEqual("Todo content 3");
		a(t.get("created_by")).shouldBeEqual(u.getId());
		a(t.getOptions().get("ver")).shouldBeEqual(1);
		a(t.getOptionValue("ver")).shouldBeEqual("1");
	}
	
	@Test
	public void internalScenario()
	{
		a(User.count()).shouldBeEqual(0);
		a(Scope.count()).shouldBeEqual(0);
		a(Todo.count()).shouldBeEqual(0);
		a(ScopesTodos.count()).shouldBeEqual(0);
		a(ScopesUsers.count()).shouldBeEqual(0);
		HashMap<String, OpOutput> map = buildBaseScenario(0);
		a(User.count()).shouldBeEqual(1);
		a(Scope.count()).shouldBeEqual(1);
		a(Todo.count()).shouldBeEqual(3);
		a(ScopesTodos.count()).shouldBeEqual(3);
		a(ScopesUsers.count()).shouldBeEqual(1);
	}
	
	@Test
	public void operationGetUserDashboard()
	{
		HashMap<String, OpOutput> map1 = buildBaseScenario(1);
		HashMap<String, OpOutput> map0 = buildBaseScenario(0);
		Scope s = (Scope) map0.get("scope").getObject();
		User u = (User) map0.get("user").getObject();
		OpOutput o = Operations.getUserDashboard(u);
		Scope s1 = (Scope) o.getChainItem("scope").getObject();
		List<ScopesTodos> sts = (List<ScopesTodos>) o.getObject();
		int count = 0;
		for(ScopesTodos st: sts){
			Todo t = st.parent(Todo.class);
			a(t.get("name")).shouldBeEqual("Todo "+count);
			count++;
		}
		a(count).shouldBeEqual(3);
	}
	
	
}
