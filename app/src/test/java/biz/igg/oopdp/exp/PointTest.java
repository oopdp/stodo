/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.exp;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import biz.igg.oopdp.exp.Point;
import org.junit.Assert;

/**
 *
 * @author salvix
 */
public class PointTest {
	
	public PointTest() {
	}
	
	@BeforeClass
	public static void setUpClass() {
	}
	
	@AfterClass
	public static void tearDownClass() {
	}
	
	@Before
	public void setUp() {
	}
	
	@After
	public void tearDown() {
	}
	
	@Test
	public void hello()
	{
		assert(true);
	}
	
	@Test
	public void createFromCoordinates()
	{
		Point p = new Point(1,2);
		String ret = p.doMeAgainAgain3();
		Assert.assertEquals("(1.0,2.0)", ret);
	}
	
	@Test
	public void createFromPoint()
	{
		Point p = new Point(1,2);
		Point clone = new Point(p);
		Assert.assertEquals(p.doMe(), p.doMe());
	}
	
	@Test
	public void swap()
	{
		Point p1 = new Point(1,2);
		Assert.assertEquals("(1.0,2.0)", p1.doMe());
		Point p2 = new Point(3,4);
		Assert.assertEquals("(3.0,4.0)", p2.doMe());
		p1.swap(p1, p2);
		Assert.assertEquals("(1.0,2.0)", p1.doMe());
		Assert.assertEquals("(3.0,4.0)", p2.doMe());
	}
	
	@Test
	public void alter()
	{
		Point p1 = new Point(1,2);
		Assert.assertEquals("(1.0,2.0)", p1.doMe());
		Point p2 = new Point(3,4);
		Assert.assertEquals("(3.0,4.0)", p2.doMe());
		p1.swap(p1, p2);
		Assert.assertEquals("(1.0,2.0)", p1.doMe());
		Assert.assertEquals("(3.0,4.0)", p2.doMe());
		Util.log(p2);
		Util.log(p1);
		p1.alter(p1, p2);
		Util.log(p2);
		Util.log(p1);
//		Assert.assertEquals("(3.0,4.0)", p1.doMe());
		Assert.assertEquals("(1.0,2.0)", p2.doMe());
	}
	
	@Test
	public void swapCoordinates()
	{
		Point p1 = new Point(1,2);
		Assert.assertEquals("(1.0,2.0)", p1.doMe());
		Point p2 = new Point(3,4);
		Assert.assertEquals("(3.0,4.0)", p2.doMe());
		p1.swapCoordinates(p2);
		Assert.assertEquals("(1.0,2.0)", p1.doMe());
		Assert.assertEquals("(4.0,3.0)", p2.doMe());
	}
}
