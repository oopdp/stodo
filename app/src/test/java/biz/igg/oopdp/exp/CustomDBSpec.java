/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.exp;

import app.models.stodo.Scope;
import app.models.stodo.User;
import static biz.igg.oopdp.exp.Operations.alterUserParamsForEncryption;
import biz.igg.oopdp.exp.auth.PasswordEncryptionServiceWrapper;
import java.util.HashMap;
import org.javalite.activeweb.DBSpec;
import org.junit.After;
import org.junit.Before;

/**
 *
 * @author salvix
 */
public abstract class CustomDBSpec extends DBSpec {
	
	@Before
	public void beforeTest()
	{
//		Util.log("BEFORE!");
	}
	
	@After
	public void afterTest()
	{
//		Util.log("AFTER");
	}
	
	public static User genUser(int seed)
	{
		User u = new User();
		HashMap<String, String> m = genUserData(seed);
		u.fromMap(m);
		u.saveIt();
		return u;
	}
	
	public static HashMap<String, String> genUserData(int seed)
	{
		HashMap<String, String> m = genSignUpData(seed);
		m.remove("password_rep");
		m.put("confirmation_code", User.generateConfirmationCode());
		alterUserParamsForEncryption(m);
		m.put("confirmed", "0");
		m.put("timezone", "UTC");
		m.put("language", "en");
		return m;
	}
	
	
	public static HashMap<String, String> genSignUpData(int seed)
	{
		HashMap<String, String> m = new HashMap<String, String>();
		m.put("fullname", "Full Name "+seed);
		m.put("username", "username"+seed);
		m.put("email", "filecondivisi+stodo+"+seed+"@gmail.com");
		m.put("password", "pwd");
		m.put("password_rep", "pwd");
		return m;
	}
	
	public static HashMap<String, String> genSignInData(int seed)
	{
		HashMap<String, String> m = new HashMap<String, String>();
		m.put("email", "filecondivisi+stodo+"+seed+"@gmail.com");
		m.put("password", "pwd");
		return m;
	}
	
	public static HashMap<String, String> genTodoData(int seed)
	{
		HashMap<String, String> m = new HashMap<String, String>();
		m.put("name", "Todo "+seed);
		m.put("content", "Todo content "+seed);
		return m;
	}
	
	public static HashMap<String, OpOutput> buildBaseScenario(int seed)
	{
		HashMap<String, OpOutput> m = new HashMap<String, OpOutput>();
		HashMap<String, String> um = genSignUpData(seed);
		OpOutput o1 = Operations.createUserFromParams(um);
		User u = (User) o1.getObject();
		OpOutput o2 = Operations.buildUserDefaultScope(u);
		Scope s = (Scope) o2.getObject();
		
		HashMap<String, String> mt0 = genTodoData(0);
		OpOutput t0 = Operations.createTodoFromParams(u, s, mt0);
		HashMap<String, String> mt1 = genTodoData(1);
		OpOutput t1 = Operations.createTodoFromParams(u, s, mt1);
		HashMap<String, String> mt2 = genTodoData(2);
		OpOutput t2 = Operations.createTodoFromParams(u, s, mt2);
		m.put("user", o1);
		m.put("scope", o2);
		m.put("task_0", t0);
		m.put("task_1", t1);
		m.put("task_2", t2);
		return m;
	}
}
