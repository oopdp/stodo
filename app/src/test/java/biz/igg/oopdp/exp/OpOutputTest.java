/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.exp;
import org.junit.Test;
import junit.framework.Assert;
/**
 *
 * @author salvix
 */
public class OpOutputTest {
	
	
	@Test
	public void OpOutput()
	{
		OpOutput o1 = new OpOutput();
		OpOutput o2 = new OpOutput();
		
		OpOutput o = new OpOutput();
		o.appendItemToChain("o1", o1);
		OpOutput expO1 = o.getChainItem("o1");
		o.appendItemToChain("o2", o2);
		OpOutput expO2 = o.getChainItem("o2");
		Assert.assertEquals(expO1, o1);
		Assert.assertEquals(expO2, o2);
	}
}
