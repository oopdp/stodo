/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.exp;

import org.junit.Test;
import junit.framework.Assert;
import org.json.simple.JSONObject;


/**
 *
 * @author salvix
 */
public class UtilTest {
	
	public UtilTest() {
	}
	
	@Test
	public void uuid()
	{
		String u = Util.uuid();
		Assert.assertEquals(36, u.length());
	}
	
	@Test
	public void toAndFromJson()
	{
		String s = "{\n" +
"    \"glossary\": {\n" +
"        \"title\": \"example glossary\",\n" +
"		\"GlossDiv\": {\n" +
"            \"title\": \"S\",\n" +
"			\"GlossList\": {\n" +
"                \"GlossEntry\": {\n" +
"                    \"ID\": \"SGML\",\n" +
"					\"SortAs\": \"SGML\",\n" +
"					\"GlossTerm\": \"Standard Generalized Markup Language\",\n" +
"					\"Acronym\": \"SGML\",\n" +
"					\"Abbrev\": \"ISO 8879:1986\",\n" +
"					\"GlossDef\": {\n" +
"                        \"para\": \"A meta-markup language, used to create markup languages such as DocBook.\",\n" +
"						\"GlossSeeAlso\": [\"GML\", \"XML\"]\n" +
"                    },\n" +
"					\"GlossSee\": \"markup\"\n" +
"                }\n" +
"            }\n" +
"        }\n" +
"    }\n" +
"}";
		JSONObject j = Util.toJson(s);
		String exp = "{\"glossary\":{\"title\":\"example glossary\",\"GlossDiv\":{\"GlossList\":{\"GlossEntry\":{\"GlossTerm\":\"Standard Generalized Markup Language\",\"GlossSee\":\"markup\",\"SortAs\":\"SGML\",\"GlossDef\":{\"para\":\"A meta-markup language, used to create markup languages such as DocBook.\",\"GlossSeeAlso\":[\"GML\",\"XML\"]},\"ID\":\"SGML\",\"Acronym\":\"SGML\",\"Abbrev\":\"ISO 8879:1986\"}},\"title\":\"S\"}}}";
		Assert.assertEquals(exp, j.toString());
		Assert.assertEquals(exp, Util.fromJson(j));
	}
	
	@Test
	public void anotherJsonExperiment()
	{
		String exp = "{\"surname\":\"B\",\"name\":\"A\"}";
		JSONObject j = new JSONObject();
		Assert.assertEquals("{}", Util.fromJson(j));
		j.put("surname", "B");
		j.put("name", "A");
		String out = Util.fromJson(j);
		Assert.assertEquals(exp, out);
		JSONObject jj = Util.toJson(out);
		Assert.assertEquals(j, jj);
	}
	

	
}
