/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.exp;

import java.util.Arrays;
import java.util.TimeZone;
import junit.framework.Assert;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Test;


/**
 *
 * @author salvix
 */
public class SandboxTest {
	
	public SandboxTest() {
	}
	
//	@Test
	public void hi()
	{
		Assert.assertTrue(true);
		System.out.println(Arrays.toString(java.util.TimeZone.getAvailableIDs()));
		System.out.println(TimeZone.getTimeZone("UTC"));
		System.out.println(Arrays.toString(java.util.Locale.getAvailableLocales()));

	}
	
	@Test
	public void toJson()
	{
		String s = "{\n" +
"    \"glossary\": {\n" +
"        \"title\": \"example glossary\",\n" +
"		\"GlossDiv\": {\n" +
"            \"title\": \"S\",\n" +
"			\"GlossList\": {\n" +
"                \"GlossEntry\": {\n" +
"                    \"ID\": \"SGML\",\n" +
"					\"SortAs\": \"SGML\",\n" +
"					\"GlossTerm\": \"Standard Generalized Markup Language\",\n" +
"					\"Acronym\": \"SGML\",\n" +
"					\"Abbrev\": \"ISO 8879:1986\",\n" +
"					\"GlossDef\": {\n" +
"                        \"para\": \"A meta-markup language, used to create markup languages such as DocBook.\",\n" +
"						\"GlossSeeAlso\": [\"GML\", \"XML\"]\n" +
"                    },\n" +
"					\"GlossSee\": \"markup\"\n" +
"                }\n" +
"            }\n" +
"        }\n" +
"    }\n" +
"}";
		JSONParser parser = new JSONParser();
		JSONObject json;
		try {
			json = (JSONObject) parser.parse(s);
		}
		catch(ParseException e){
			json = new JSONObject();
		}
		System.out.println(json);
	}
	
	public void fromJson()
	{
		
	}
}
